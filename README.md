Chill CI docker image
======================

This image should be able to run chill tests on chill bundles.

This image ships with [composer](https://getcomposer.org) and [phpunit](https://phpunit.de). 

Usage: 
======

```
docker pull chill/ci-image:php-5.6
docker run --rm -it chill/ci-image
```

Then, inside the container, you : 

* clone a git repo ;
* run `composer` to install dependencies ;
* run `phpunit`
* run `php` as command line

Use this container to run composer
==================================

In the current directory :

```
docker run --rm -v `pwd`:/app -it chill/ci-image:php-5.6 composer update --ansi
```

